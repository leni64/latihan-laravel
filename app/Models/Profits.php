<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;


class Profits extends Model
{
    use HasFactory;
     /**
     * fillable
     * 
     * @var array
     */
    protected $fillable =[
        'transaction_id','total'
    ];

    /**
     * transactions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->BelongTo(Transactions::class);
    }
     /**
     * createdAt
     * 
     * @return attribute
     */
    protected function createAt(): Attribute
    {
        return Attribute::make(
            get:fn ($value) =>Carbon::parse($value)->format('d-M-Y H:i:s'),
        );
    }
}
