<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable =[
        'name','no_telp','address'
    ];
    
    /**
     * transactions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->HasMany(Transactions::class);
    }
}
